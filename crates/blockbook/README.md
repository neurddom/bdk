# BDK Blockbook

BDK Blockbook extends [`blockbook`](crate::blockbook) to update [`bdk_chain`] structures
from a Blockbook server.

## Usage

There are two versions of the extension trait (blocking and async).

For blocking-only:
```toml
bdk_blockbook = { version = "0.1", features = ["blocking"] }
```

For async-only:
```toml
bdk_blockbook = { version = "0.1", features = ["async"] }
```

For async-only (with https):
```toml
bdk_blockbook = { version = "0.1", features = ["async-https"] }
```

To use the extension traits:
```rust
// for blocking
use bdk_blockbook::BlockbookExt;
// for async
// use bdk_blockbook::BlockbookAsyncExt;
```

For full examples, refer to [`example-crates/wallet_blockbook`](https://github.com/bitcoindevkit/bdk/tree/master/example-crates/wallet_blockbook) (blocking) and [`example-crates/wallet_blockbook_async`](https://github.com/bitcoindevkit/bdk/tree/master/example-crates/wallet_blockbook_async).
