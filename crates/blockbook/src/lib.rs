#![doc = include_str!("../README.md")]

pub use blockbook;

// #[cfg(feature = "blocking")]
// mod blocking_ext;
// #[cfg(feature = "blocking")]
// pub use blocking_ext::*;

#[cfg(feature = "async")]
mod async_ext;
#[cfg(feature = "async")]
pub use async_ext::*;

const ASSUME_FINAL_DEPTH: u32 = 15;
