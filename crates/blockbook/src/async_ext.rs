use async_trait::async_trait;
use bdk_chain::collections::btree_map;
use bdk_chain::{
    bitcoin::{OutPoint, ScriptBuf, Txid},
    collections::{BTreeMap, BTreeSet},
    local_chain::{self, CheckPoint},
    BlockId, ConfirmationTimeAnchor, TxGraph,
};
use blockbook::Error;
use futures::{stream::FuturesOrdered, TryStreamExt};

use crate::ASSUME_FINAL_DEPTH;

/// Trait to extend the functionality of [`blockbook::Blockbook`].
///
/// Refer to [crate-level documentation] for more.
///
/// [crate-level documentation]: crate
#[cfg_attr(target_arch = "wasm32", async_trait(?Send))]
#[cfg_attr(not(target_arch = "wasm32"), async_trait)]
pub trait BlockbookAsyncExt {
    /// Prepare an [`LocalChain`] update with blocks fetched from Blockbook.
    ///
    /// * `prev_tip` is the previous tip of [`LocalChain::tip`].
    /// * `get_heights` is the block heights that we are interested in fetching from Blockbook.
    ///
    /// The result of this method can be applied to [`LocalChain::apply_update`].
    ///
    /// [`LocalChain`]: bdk_chain::local_chain::LocalChain
    /// [`LocalChain::tip`]: bdk_chain::local_chain::LocalChain::tip
    /// [`LocalChain::apply_update`]: bdk_chain::local_chain::LocalChain::apply_update
    #[allow(clippy::result_large_err)]
    async fn update_local_chain(
        &self,
        local_tip: Option<CheckPoint>,
        request_heights: impl IntoIterator<IntoIter = impl Iterator<Item = u32> + Send> + Send,
    ) -> Result<local_chain::Update, Error>;

    /// Scan Blockbook for the data specified and return a [`TxGraph`] and a map of last active
    /// indices.
    ///
    /// * `keychain_spks`: keychains that we want to scan transactions for
    /// * `txids`: transactions for which we want updated [`ConfirmationTimeAnchor`]s
    /// * `outpoints`: transactions associated with these outpoints (residing, spending) that we
    ///     want to include in the update
    ///
    /// The scan for each keychain stops after a gap of `stop_gap` script pubkeys with no associated
    /// transactions. `parallel_requests` specifies the max number of HTTP requests to make in
    /// parallel.
    #[allow(clippy::result_large_err)]
    async fn update_tx_graph<K: Ord + Clone + Send>(
        &self,
        keychain_spks: BTreeMap<
            K,
            impl IntoIterator<IntoIter = impl Iterator<Item = (u32, ScriptBuf)> + Send> + Send,
        >,
        netowrk: &bitcoin::Network,
        txids: impl IntoIterator<IntoIter = impl Iterator<Item = Txid> + Send> + Send,
        outpoints: impl IntoIterator<IntoIter = impl Iterator<Item = OutPoint> + Send> + Send,
        stop_gap: usize,
        parallel_requests: usize,
    ) -> Result<(TxGraph<ConfirmationTimeAnchor>, BTreeMap<K, u32>), Error>;

    /// Convenience method to call [`update_tx_graph`] without requiring a keychain.
    ///
    /// [`update_tx_graph`]: BlockbookAsyncExt::update_tx_graph
    #[allow(clippy::result_large_err)]
    async fn update_tx_graph_without_keychain(
        &self,
        misc_spks: impl IntoIterator<IntoIter = impl Iterator<Item = ScriptBuf> + Send> + Send,
        network: &bitcoin::Network,
        txids: impl IntoIterator<IntoIter = impl Iterator<Item = Txid> + Send> + Send,
        outpoints: impl IntoIterator<IntoIter = impl Iterator<Item = OutPoint> + Send> + Send,
        parallel_requests: usize,
    ) -> Result<TxGraph<ConfirmationTimeAnchor>, Error> {
        self.update_tx_graph(
            [(
                (),
                misc_spks
                    .into_iter()
                    .enumerate()
                    .map(|(i, spk)| (i as u32, spk)),
            )]
            .into(),
            network,
            txids,
            outpoints,
            usize::MAX,
            parallel_requests,
        )
        .await
        .map(|(g, _)| g)
    }
}

#[cfg_attr(target_arch = "wasm32", async_trait(?Send))]
#[cfg_attr(not(target_arch = "wasm32"), async_trait)]
impl BlockbookAsyncExt for blockbook::Client {
    async fn update_local_chain(
        &self,
        local_tip: Option<CheckPoint>,
        request_heights: impl IntoIterator<IntoIter = impl Iterator<Item = u32> + Send> + Send,
    ) -> Result<local_chain::Update, Error> {
        let request_heights = request_heights.into_iter().collect::<BTreeSet<_>>();
        let new_tip_height = self
            .status()
            .await?
            .blockbook
            .best_height
            .to_consensus_u32();

        // atomically fetch last 10 blocks from blockbook
        let mut fetched_blocks = {
            (new_tip_height - 10..=new_tip_height)
                .map(|height| {
                    let client = self.clone();
                    async move {
                        client
                            .block_hash(&blockbook::Height::from_consensus(height).unwrap())
                            .await
                            .map(|header| (height, header))
                    }
                })
                .collect::<FuturesOrdered<_>>()
                .try_collect::<BTreeMap<u32, blockbook::BlockHash>>()
                .await
                .unwrap()
        };

        // fetch heights that the caller is interested in
        for height in request_heights {
            // do not fetch blocks higher than remote tip
            if height > new_tip_height {
                continue;
            }
            // only fetch what is missing
            if let btree_map::Entry::Vacant(entry) = fetched_blocks.entry(height) {
                let hash = self
                    .block_hash(&blockbook::Height::from_consensus(height).unwrap())
                    .await?;
                entry.insert(hash);
            }
        }

        // find the earliest point of agreement between local chain and fetched chain
        let earliest_agreement_cp = {
            let mut earliest_agreement_cp = Option::<CheckPoint>::None;

            if let Some(local_tip) = local_tip {
                let local_tip_height = local_tip.height();
                for local_cp in local_tip.iter() {
                    let local_block = local_cp.block_id();

                    // the updated hash (block hash at this height after the update), can either be:
                    // 1. a block that already existed in `fetched_blocks`
                    // 2. a block that exists locally and atleast has a depth of ASSUME_FINAL_DEPTH
                    // 3. otherwise we can freshly fetch the block from remote, which is safe as it
                    //    is guaranteed that this would be at or below ASSUME_FINAL_DEPTH from the
                    //    remote tip
                    let updated_hash = match fetched_blocks.entry(local_block.height) {
                        btree_map::Entry::Occupied(entry) => *entry.get(),
                        btree_map::Entry::Vacant(entry) => *entry.insert(
                            if local_tip_height - local_block.height >= ASSUME_FINAL_DEPTH {
                                local_block.hash
                            } else {
                                self.block_hash(
                                    &blockbook::Height::from_consensus(local_block.height).unwrap(),
                                )
                                .await?
                            },
                        ),
                    };

                    // since we may introduce blocks below the point of agreement, we cannot break
                    // here unconditionally - we only break if we guarantee there are no new heights
                    // below our current local checkpoint
                    if local_block.hash == updated_hash {
                        earliest_agreement_cp = Some(local_cp);

                        let first_new_height = *fetched_blocks
                            .keys()
                            .next()
                            .expect("must have atleast one new block");
                        if first_new_height >= local_block.height {
                            break;
                        }
                    }
                }
            }

            earliest_agreement_cp
        };

        let tip = {
            // first checkpoint to use for the update chain
            let first_cp = match earliest_agreement_cp {
                Some(cp) => cp,
                None => {
                    let (&height, &hash) = fetched_blocks
                        .iter()
                        .next()
                        .expect("must have atleast one new block");
                    CheckPoint::new(BlockId { height, hash })
                }
            };
            // transform fetched chain into the update chain
            fetched_blocks
                // we exclude anything at or below the first cp of the update chain otherwise
                // building the chain will fail
                .split_off(&(first_cp.height() + 1))
                .into_iter()
                .map(|(height, hash)| BlockId { height, hash })
                .fold(first_cp, |prev_cp, block| {
                    prev_cp.push(block).expect("must extend checkpoint")
                })
        };

        Ok(local_chain::Update {
            tip,
            introduce_older_blocks: true,
        })
    }

    async fn update_tx_graph<K: Ord + Clone + Send>(
        &self,
        keychain_spks: BTreeMap<
            K,
            impl IntoIterator<IntoIter = impl Iterator<Item = (u32, ScriptBuf)> + Send> + Send,
        >,
        network: &bitcoin::Network,
        txids: impl IntoIterator<IntoIter = impl Iterator<Item = Txid> + Send> + Send,
        outpoints: impl IntoIterator<IntoIter = impl Iterator<Item = OutPoint> + Send> + Send,
        stop_gap: usize,
        parallel_requests: usize,
    ) -> Result<(TxGraph<ConfirmationTimeAnchor>, BTreeMap<K, u32>), Error> {
        let parallel_requests = Ord::max(parallel_requests, 1);
        let mut graph = TxGraph::<ConfirmationTimeAnchor>::default();
        let mut last_active_indexes = BTreeMap::<K, u32>::new();

        for (keychain, spks) in keychain_spks {
            let mut spks = spks.into_iter();
            let mut last_index = Option::<u32>::None;
            let mut last_active_index = Option::<u32>::None;

            // first collect all txids
            let txids: Vec<blockbook::Txid> = loop {
                let mut txids = Vec::new();
                let handles = spks
                    .by_ref()
                    .take(parallel_requests)
                    .map(|(spk_index, spk)| {
                        let client = self.clone();
                        async move {
                            let address = bitcoin::Address::new(
                                *network,
                                bitcoin::address::Payload::from_script(&spk).unwrap(),
                            );
                            client
                                .address_info(&address)
                                .await
                                .map(|info| (spk_index, info.txids))
                        }
                    })
                    .collect::<FuturesOrdered<_>>();

                if handles.is_empty() {
                    break txids;
                }

                for (index, fresh_txids) in handles
                    .try_collect::<Vec<(u32, Vec<blockbook::Txid>)>>()
                    .await?
                {
                    last_index = Some(index);
                    if !fresh_txids.is_empty() {
                        last_active_index = Some(index);
                    }
                    txids.extend(fresh_txids);
                }

                if last_index > last_active_index.map(|i| i + stop_gap as u32) {
                    break txids;
                }
            };

            if let Some(last_active_index) = last_active_index {
                last_active_indexes.insert(keychain, last_active_index);
            }

            // next collect all transactions
            let mut txids = txids.into_iter();
            loop {
                let handles = txids
                    .by_ref()
                    .take(parallel_requests)
                    .map(|txid| {
                        let client = self.clone();
                        async move {
                            let tx = client.transaction_specific(&txid).await?;
                            let tx_anchor_info = if tx.blockhash.is_some() {
                                Some(client.transaction(&txid).await?)
                            } else {
                                None
                            };
                            Ok::<_, Error>((tx, tx_anchor_info))
                        }
                    })
                    .collect::<FuturesOrdered<_>>();

                if handles.is_empty() {
                    break;
                }

                for (tx, tx_anchor_info) in handles
                    .try_collect::<Vec<(
                        blockbook::TransactionSpecific,
                        Option<blockbook::Transaction>,
                    )>>()
                    .await?
                {
                    if let Some(inner_tx) = tx_anchor_info {
                        if let Some(anchor) = extract_anchor_info(inner_tx) {
                            let _ = graph.insert_anchor(tx.txid, anchor);
                        }
                    }
                    let _ = graph.insert_tx(tx.into());
                }
            }
        }

        let mut txids = txids.into_iter();
        loop {
            let handles = txids
                .by_ref()
                .take(parallel_requests)
                .filter(|&txid| graph.get_tx(txid).is_none())
                .map(|txid| {
                    let client = self.clone();
                    async move { client.transaction(&txid).await }
                })
                .collect::<FuturesOrdered<_>>();

            if handles.is_empty() {
                break;
            }

            for tx in handles.try_collect::<Vec<blockbook::Transaction>>().await? {
                if let blockbook::Transaction {
                    block_height: Some(height),
                    block_hash: Some(hash),
                    block_time: time, // Blockbook sets this even for unconfirmed transactions
                    ..
                } = tx
                {
                    let _ = graph.insert_anchor(
                        tx.txid,
                        bdk_chain::ConfirmationTimeAnchor {
                            anchor_block: BlockId {
                                height: height.to_consensus_u32(),
                                hash,
                            },
                            confirmation_height: height.to_consensus_u32(),
                            confirmation_time: time.to_consensus_u32().into(),
                        },
                    );
                }
            }
        }

        for op in outpoints.into_iter() {
            if graph.get_tx(op.txid).is_none() {
                let tx = self.transaction_specific(&op.txid).await?;
                if tx.blockhash.is_some() {
                    if let Some(anchor) = extract_anchor_info(self.transaction(&tx.txid).await?) {
                        let _ = graph.insert_anchor(tx.txid, anchor);
                    }
                }
                let _ = graph.insert_tx(tx.into());
            }

            if let blockbook::Vout {
                spent_tx_id: Some(id),
                ..
            } = self.transaction(&op.txid).await?.vout[op.vout as usize]
            {
                if graph.get_tx(id).is_none() {
                    let tx = self.transaction_specific(&id).await?;
                    if tx.blockhash.is_some() {
                        if let Some(anchor) = extract_anchor_info(self.transaction(&tx.txid).await?)
                        {
                            let _ = graph.insert_anchor(tx.txid, anchor);
                        }
                    }
                    let _ = graph.insert_tx(tx.into());
                }
            }
        }

        Ok((graph, last_active_indexes))
    }
}

fn extract_anchor_info(tx: blockbook::Transaction) -> Option<bdk_chain::ConfirmationTimeAnchor> {
    let height = tx.block_height?.to_consensus_u32();
    Some(bdk_chain::ConfirmationTimeAnchor {
        anchor_block: BlockId {
            height,
            hash: tx.block_hash?,
        },
        confirmation_height: height,
        confirmation_time: tx.block_time.to_consensus_u32().into(),
    })
}
